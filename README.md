# unity_physics_csp

Demo project showing basic implementation of client-side prediction in Unity, so real networking happens, all network traffic is simulated in a single Unity instance.

Improvements made by Aran Koning, using PhysicsScenes to improve accuracy of physics simulation. Open in Unity 2019.1 or higher